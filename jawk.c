#include <stdio.h>
#include <stdlib.h>

int		main(int argc, char **argv)
{
	int		a;
	int		b;

	a = 0;
	b = 2;
	if (argc == 1)
    {
        printf("Usage : jawk <-/+ number> <string>\n");
        printf("For example : jawk -5 1 2 3 4 5 6 7 8 9 0 --> 1 2 3 4 5\n");
        printf("              jawk +5 1 2 3 4 5 6 7 8 9 0 --> 6 7 8 9 0\n");
        printf("              jawk 5 1 2 3 4 5 6 7 8 9 0  --> 5\n");
        return(0);
    }
	if (argv[1][0] == '-')
	{
		a = 2 + atoi(argv[1] + 1);
		while (b < a)
		{
		    if (argv[b])
                printf("%s ", argv[b]);
			b++;
		}
	}
	if (argv[1][0] == '+')
	{
		a = atoi(argv[1] + 1);
		b = a + 2;
		while (b < argc)
		{
		    if (argv[b])
                printf("%s ", argv[b]);
			b++;
		}
	}
	if ('0' <= argv[1][0] && argv[1][0] <= '9')
    {
        a = atoi(argv[1]);
        b = a + 1;
        if (argv[b])
            printf("%s", argv[b]);
    }
    printf("\n");
    return(0);
}
