all:
	gcc jawk.c -o jawk

clean:
	rm jawk

fclean: clean

re: fclean all

system: all
	sudo cp jawk /bin/
